<?php

namespace Tests\Drink;

use PHPUnit\Framework\TestCase;
use Src\Drink\DrinkService;
use Src\Drink\GenerateInstructionsForDrinkMakerServiceI;
use Src\Message\MessageService;
use Src\Message\MessageServiceI;
use Src\Model\Drink;
use Src\Model\Order;
use Src\Stock\StockServiceI;
use Src\Validator\AssertEnoughMoneyIsProvidedServiceI;


class DrinkServiceTest extends TestCase
{

    public function test_when_ordering_tea_1_sugar()
    {
        //GIVEN
        $order = new Order(Drink::TEA, 1, 2, false);
        $assertEnoughMoneyIsProvidedService = $this->getMockBuilder(AssertEnoughMoneyIsProvidedServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $messageService = $this->getMockBuilder(MessageServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $generateInstructionsForDrinkMakerService = $this->getMockBuilder(GenerateInstructionsForDrinkMakerServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $generateInstructionsForDrinkMakerService
            ->method('generate')
            ->with(Drink::TEA, 1, false)
            ->willReturn("T:1:0");
        $stockService = $this->getMockBuilder(StockServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $stockService
            ->expects($this->once())
            ->method('sell')
            ->with(Drink::TEA);

        //WHEN
        $service = new DrinkService(
            $assertEnoughMoneyIsProvidedService,
            $messageService,
            $generateInstructionsForDrinkMakerService,
            $stockService
        );
        $actual = $service->order($order);
        //THEN
        $this->assertEquals("T:1:0", $actual);
    }

    public function test_when_ordering_choco_no_sugar()
    {
        //GIVEN
        $order = new Order(Drink::CHOCO, 0, 2, false);
        $assertEnoughMoneyIsProvidedService = $this->getMockBuilder(AssertEnoughMoneyIsProvidedServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $messageService = $this->getMockBuilder(MessageServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $generateInstructionsForDrinkMakerService = $this->getMockBuilder(GenerateInstructionsForDrinkMakerServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $generateInstructionsForDrinkMakerService
            ->method('generate')
            ->with(Drink::CHOCO, 0, false)
            ->willReturn("H::");
        $stockService = $this->getMockBuilder(StockServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $stockService
            ->expects($this->once())
            ->method('sell')
            ->with(Drink::CHOCO);
        //WHEN
        $service = new DrinkService(
            $assertEnoughMoneyIsProvidedService,
            $messageService,
            $generateInstructionsForDrinkMakerService,
            $stockService
        );
        $actual = $service->order($order);
        //THEN
        $this->assertEquals("H::", $actual);
    }

    public function test_when_ordering_coffee_2_sugar()
    {
        //GIVEN
        $order = new Order(Drink::COFFEE, 2, 2, false);
        $assertEnoughMoneyIsProvidedService = $this->getMockBuilder(AssertEnoughMoneyIsProvidedServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $messageService = $this->getMockBuilder(MessageServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $generateInstructionsForDrinkMakerService = $this->getMockBuilder(GenerateInstructionsForDrinkMakerServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $generateInstructionsForDrinkMakerService
            ->method('generate')
            ->with(Drink::COFFEE, 2, false)
            ->willReturn("C:2:0");
        $stockService = $this->getMockBuilder(StockServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $stockService
            ->expects($this->once())
            ->method('sell')
            ->with(Drink::COFFEE);
        //WHEN
        $service = new DrinkService(
            $assertEnoughMoneyIsProvidedService,
            $messageService,
            $generateInstructionsForDrinkMakerService,
            $stockService
        );
        $actual = $service->order($order);
        //THEN
        $this->assertEquals("C:2:0", $actual);
    }

    public function test_when_ordering_coffee_1_sugar_no_enough_money()
    {
        //GIVEN
        $order = new Order(Drink::COFFEE, 1, 0.2, false);
        $message = "Coffee costs 0.60, 0.40 is missing";
        $assertEnoughMoneyIsProvidedService = $this->getMockBuilder(AssertEnoughMoneyIsProvidedServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $assertEnoughMoneyIsProvidedService
            ->method('assert')
            ->with(Drink::COFFEE, 0.2)
            ->willThrowException(new \Exception($message));
        $messageService = $this->getMockBuilder(MessageServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $messageService
            ->method('sendMessage')
            ->with($message)
            ->willReturn(MessageService::MESSAGE_PREFIX.":".$message);
        $generateInstructionsForDrinkMakerService = $this->getMockBuilder(GenerateInstructionsForDrinkMakerServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $stockService = $this->getMockBuilder(StockServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();

        //WHEN
        $service = new DrinkService(
            $assertEnoughMoneyIsProvidedService,
            $messageService,
            $generateInstructionsForDrinkMakerService,
            $stockService
        );
        $actual = $service->order($order);
        //THEN
        $this->assertEquals(MessageService::MESSAGE_PREFIX.":".$message, $actual);
    }

    public function test_when_ordering_tea_1_sugar_no_enough_money()
    {
        //GIVEN
        $order = new Order(Drink::TEA, 1, 0.2, false);
        $message = "Tea costs 0.40, 0.20 is missing";
        $assertEnoughMoneyIsProvidedService = $this->getMockBuilder(AssertEnoughMoneyIsProvidedServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $assertEnoughMoneyIsProvidedService
            ->method('assert')
            ->with(Drink::TEA, 0.2)
            ->willThrowException(new \Exception($message));
        $messageService = $this->getMockBuilder(MessageServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $messageService
            ->method('sendMessage')
            ->with($message)
            ->willReturn(MessageService::MESSAGE_PREFIX.":".$message);
        $generateInstructionsForDrinkMakerService = $this->getMockBuilder(GenerateInstructionsForDrinkMakerServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $stockService = $this->getMockBuilder(StockServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        //WHEN
        $service = new DrinkService(
            $assertEnoughMoneyIsProvidedService,
            $messageService,
            $generateInstructionsForDrinkMakerService,
            $stockService
        );
        $actual = $service->order($order);
        //THEN
        $this->assertEquals(MessageService::MESSAGE_PREFIX.":"."Tea costs 0.40, 0.20 is missing", $actual);
    }

    public function test_when_ordering_choco_no_enough_money()
    {
        //GIVEN
        $order = new Order(Drink::CHOCO, 0, 0.2, false);
        $message = "Chocolate costs 0.50, 0.30 is missing";
        $assertEnoughMoneyIsProvidedService = $this->getMockBuilder(AssertEnoughMoneyIsProvidedServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $assertEnoughMoneyIsProvidedService
            ->method('assert')
            ->with(Drink::CHOCO, 0.2)
            ->willThrowException(new \Exception($message));
        $messageService = $this->getMockBuilder(MessageServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $messageService
            ->method('sendMessage')
            ->with($message)
            ->willReturn(MessageService::MESSAGE_PREFIX.":".$message);
        $generateInstructionsForDrinkMakerService = $this->getMockBuilder(GenerateInstructionsForDrinkMakerServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $stockService = $this->getMockBuilder(StockServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        //WHEN
        $service = new DrinkService(
            $assertEnoughMoneyIsProvidedService,
            $messageService,
            $generateInstructionsForDrinkMakerService,
            $stockService
        );
        $actual = $service->order($order);
        //THEN
        $this->assertEquals(MessageService::MESSAGE_PREFIX.":".$message, $actual);
    }

    public function test_when_ordering_orange_juice(){
        //GIVEN
        $order = new Order(Drink::ORANGE, 0, 2, false);
        $assertEnoughMoneyIsProvidedService = $this->getMockBuilder(AssertEnoughMoneyIsProvidedServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $messageService = $this->getMockBuilder(MessageServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $generateInstructionsForDrinkMakerService = $this->getMockBuilder(GenerateInstructionsForDrinkMakerServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $generateInstructionsForDrinkMakerService
            ->method('generate')
            ->with(Drink::ORANGE, 0, false)
            ->willReturn("O::");
        $stockService = $this->getMockBuilder(StockServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $stockService
            ->expects($this->once())
            ->method('sell')
            ->with(Drink::ORANGE);
        //WHEN
        $service = new DrinkService(
            $assertEnoughMoneyIsProvidedService,
            $messageService,
            $generateInstructionsForDrinkMakerService,
            $stockService
        );
        $actual = $service->order($order);
        //THEN
        $this->assertEquals("O::", $actual);
    }

    public function test_when_ordering_coffee_extraHot(){
        //GIVEN
        $order = new Order(Drink::COFFEE, 0, 2, true);
        $assertEnoughMoneyIsProvidedService = $this->getMockBuilder(AssertEnoughMoneyIsProvidedServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $messageService = $this->getMockBuilder(MessageServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $generateInstructionsForDrinkMakerService = $this->getMockBuilder(GenerateInstructionsForDrinkMakerServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $generateInstructionsForDrinkMakerService
            ->method('generate')
            ->with(Drink::COFFEE, 0, true)
            ->willReturn("Ch::");
        $stockService = $this->getMockBuilder(StockServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $stockService
            ->expects($this->once())
            ->method('sell')
            ->with(Drink::COFFEE);
        //WHEN
        $service = new DrinkService(
            $assertEnoughMoneyIsProvidedService,
            $messageService,
            $generateInstructionsForDrinkMakerService,
            $stockService
        );
        $actual = $service->order($order);
        //THEN
        $this->assertEquals("Ch::", $actual);
    }

    public function test_when_ordering_choco_1_sugar_extraHot(){
        //GIVEN
        $order = new Order(Drink::CHOCO, 1, 2, true);
        $assertEnoughMoneyIsProvidedService = $this->getMockBuilder(AssertEnoughMoneyIsProvidedServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $messageService = $this->getMockBuilder(MessageServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $generateInstructionsForDrinkMakerService = $this->getMockBuilder(GenerateInstructionsForDrinkMakerServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $generateInstructionsForDrinkMakerService
            ->method('generate')
            ->with(Drink::CHOCO, 1, true)
            ->willReturn("Hh:1:0");
        $stockService = $this->getMockBuilder(StockServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $stockService
            ->expects($this->once())
            ->method('sell')
            ->with(Drink::CHOCO);
        //WHEN
        $service = new DrinkService(
            $assertEnoughMoneyIsProvidedService,
            $messageService,
            $generateInstructionsForDrinkMakerService,
            $stockService
        );
        $actual = $service->order($order);
        //THEN
        $this->assertEquals("Hh:1:0", $actual);
    }

    public function test_when_ordering_tea_2_sugar_extraHot(){
        //GIVEN
        $order = new Order(Drink::TEA, 2, 2, true);
        $assertEnoughMoneyIsProvidedService = $this->getMockBuilder(AssertEnoughMoneyIsProvidedServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $messageService = $this->getMockBuilder(MessageServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $generateInstructionsForDrinkMakerService = $this->getMockBuilder(GenerateInstructionsForDrinkMakerServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $generateInstructionsForDrinkMakerService
            ->method('generate')
            ->with(Drink::TEA, 2, true)
            ->willReturn("Th:2:0");
        $stockService = $this->getMockBuilder(StockServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $stockService
            ->expects($this->once())
            ->method('sell')
            ->with(Drink::TEA);
        //WHEN
        $service = new DrinkService(
            $assertEnoughMoneyIsProvidedService,
            $messageService,
            $generateInstructionsForDrinkMakerService,
            $stockService
        );
        $actual = $service->order($order);
        //THEN
        $this->assertEquals("Th:2:0", $actual);
    }

}