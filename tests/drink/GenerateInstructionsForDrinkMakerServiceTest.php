<?php


namespace Tests;


use PHPUnit\Framework\TestCase;
use Src\Drink\GenerateInstructionsForDrinkMakerService;
use Src\Model\Drink;


class GenerateInstructionsForDrinkMakerServiceTest extends TestCase
{
    public function test_when_ordering_coffee_1_sugar()
    {
        //GIVEN
        $drink = Drink::COFFEE;
        $sugar = 1;
        $extraHot = false;
        //WHEN
        $service = new GenerateInstructionsForDrinkMakerService();
        $actual = $service->generate($drink, $sugar, $extraHot);
        //THEN
        $this->assertEquals("C:1:0", $actual);
    }

    public function test_when_ordering_coffee_2_sugar()
    {
        //GIVEN
        $drink = Drink::COFFEE;
        $sugar = 2;
        $extraHot = false;
        //WHEN
        $service = new GenerateInstructionsForDrinkMakerService();
        $actual = $service->generate($drink, $sugar, $extraHot);
        //THEN
        $this->assertEquals("C:2:0", $actual);
    }

    public function test_when_ordering_coffee_no_sugar()
    {
        //GIVEN
        $drink = Drink::COFFEE;
        $sugar = 0;
        $extraHot = false;
        //WHEN
        $service = new GenerateInstructionsForDrinkMakerService();
        $actual = $service->generate($drink, $sugar, $extraHot);
        //THEN
        $this->assertEquals("C::", $actual);
    }

    public function test_when_ordering_tea_1_sugar()
    {
        //GIVEN
        $drink = Drink::TEA;
        $sugar = 1;
        $extraHot = false;
        //WHEN
        $service = new GenerateInstructionsForDrinkMakerService();
        $actual = $service->generate($drink, $sugar, $extraHot);
        //THEN
        $this->assertEquals("T:1:0", $actual);
    }

    public function test_when_ordering_tea_2_sugar()
    {
        //GIVEN
        $drink = Drink::TEA;
        $sugar = 2;
        $extraHot = false;
        //WHEN
        $service = new GenerateInstructionsForDrinkMakerService();
        $actual = $service->generate($drink, $sugar, $extraHot);
        //THEN
        $this->assertEquals("T:2:0", $actual);
    }

    public function test_when_ordering_tea_no_sugar()
    {
        //GIVEN
        $drink = Drink::TEA;
        $sugar = 0;
        $extraHot = false;
        //WHEN
        $service = new GenerateInstructionsForDrinkMakerService();
        $actual = $service->generate($drink, $sugar, $extraHot);
        //THEN
        $this->assertEquals("T::", $actual);
    }

    public function test_when_ordering_choco_1_sugar()
    {
        //GIVEN
        $drink = Drink::CHOCO;
        $sugar = 1;
        $extraHot = false;
        //WHEN
        $service = new GenerateInstructionsForDrinkMakerService();
        $actual = $service->generate($drink, $sugar, $extraHot);
        //THEN
        $this->assertEquals("H:1:0", $actual);
    }

    public function test_when_ordering_choco_2_sugar()
    {
        //GIVEN
        $drink = Drink::CHOCO;
        $sugar = 2;
        $extraHot = false;
        //WHEN
        $service = new GenerateInstructionsForDrinkMakerService();
        $actual = $service->generate($drink, $sugar, $extraHot);
        //THEN
        $this->assertEquals("H:2:0", $actual);
    }

    public function test_when_ordering_choco_no_sugar()
    {
        //GIVEN
        $drink = Drink::CHOCO;
        $sugar = 0;
        $extraHot = false;
        //WHEN
        $service = new GenerateInstructionsForDrinkMakerService();
        $actual = $service->generate($drink, $sugar, $extraHot);
        //THEN
        $this->assertEquals("H::", $actual);
    }

    public function test_when_ordering_orange_juice(){
        //GIVEN
        $drink = Drink::ORANGE;
        $sugar = 1;
        $extraHot = false;
        //WHEN
        $service = new GenerateInstructionsForDrinkMakerService();
        $actual = $service->generate($drink, $sugar, $extraHot);
        //THEN
        $this->assertEquals("O::", $actual);
    }

    public function test_when_ordering_coffee_extraHot(){
        //GIVEN
        $drink = Drink::COFFEE;
        $sugar = 0;
        $extraHot = true;
        //WHEN
        $service = new GenerateInstructionsForDrinkMakerService();
        $actual = $service->generate($drink, $sugar, $extraHot);
        //THEN
        $this->assertEquals("Ch::", $actual);
    }

    public function test_when_ordering_choco_1_sugar_extraHot(){
        //GIVEN
        $drink = Drink::CHOCO;
        $sugar = 1;
        $extraHot = true;
        //WHEN
        $service = new GenerateInstructionsForDrinkMakerService();
        $actual = $service->generate($drink, $sugar, $extraHot);
        //THEN
        $this->assertEquals("Hh:1:0", $actual);
    }

    public function test_when_ordering_tea_2_sugar_extraHot(){
        //GIVEN
        $drink = Drink::TEA;
        $sugar = 2;
        $extraHot = true;
        //WHEN
        $service = new GenerateInstructionsForDrinkMakerService();
        $actual = $service->generate($drink, $sugar, $extraHot);
        //THEN
        $this->assertEquals("Th:2:0", $actual);
    }

}