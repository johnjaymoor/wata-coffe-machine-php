<?php

namespace Tests\Message;

use PHPUnit\Framework\TestCase;
use Src\Message\MessageService;

class MessageServiceTest extends TestCase{
    public function test_when_sending_message()
    {
         //GIVEN
         $message = "message";
         //WHEN
         $service = new MessageService();
         $actual = $service->sendMessage($message);
         //THEN
         $this->assertEquals("M:".$message, $actual);
    }  
}