<?php


namespace Tests\Stock;


use PHPUnit\Framework\TestCase;
use Src\Model\Drink;
use Src\Stock\LoadStockServiceI;
use Src\Stock\SaveStockServiceI;
use Src\Stock\StockService;


class StockServiceTest extends TestCase
{
    public function test_that_sell_calls_load_data_service()
    {
        //GIVEN
        $drink = Drink::COFFEE;
        $loadStockService = $this->getMockBuilder(LoadStockServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $loadStockService
            ->method('load')
            ->willReturn([1,2,3,4]);
        $saveStockService = $this->getMockBuilder(SaveStockServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        //EXPECTS
        $loadStockService
            ->expects($this->once())
            ->method('load');
        //WHEN
        $service = new StockService(
            $loadStockService,
            $saveStockService
        );
        $service->sell($drink);
    }

    public function test_that_sell_calls_save_data_service()
    {
        //GIVEN
        $drink = Drink::COFFEE;
        $loadStockService = $this->getMockBuilder(LoadStockServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $loadStockService
            ->method('load')
            ->willReturn([2,2,2,2]);
        $saveStockService = $this->getMockBuilder(SaveStockServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        //EXPECTS
        $saveStockService
            ->expects($this->once())
            ->method('save')
            ->with([3,2,2,2]);
        //WHEN
        $service = new StockService(
            $loadStockService,
            $saveStockService
        );
        $service->sell($drink);
    }

    public function test_that_printSellingReport_is_printed()
    {
        //GIVEN
        $loadStockService = $this->getMockBuilder(LoadStockServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        $loadStockService
            ->method('load')
            ->willReturn([2,2,2,2]);
        $saveStockService = $this->getMockBuilder(SaveStockServiceI::class)
            ->disableOriginalConstructor()
            ->getMock();
        //EXPECTS
        $this->expectOutputString('Coffee:2 Tea:2 Chocolate:2 Orange juice:2 Total:4.20');
        //WHEN
        $service = new StockService(
            $loadStockService,
            $saveStockService
        );
        $service->printSellingReport();
    }

}