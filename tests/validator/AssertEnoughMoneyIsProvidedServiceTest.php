<?php


namespace Tests\Validator;


use PHPUnit\Framework\TestCase;
use Src\Model\Drink;
use Src\Validator\AssertEnoughMoneyIsProvidedService;


class AssertEnoughMoneyIsProvidedServiceTest extends TestCase
{
    public function testThat_when_money_is_not_enough_for_coffee_return_Exception()
    {
        //GIVEN
        $drink = Drink::COFFEE;
        $money = 0.55;
        $message = Drink::$names[$drink]." costs ". Drink::$prices[$drink] . ", 0.05 is missing";
        //WHEN
        $service = new AssertEnoughMoneyIsProvidedService();
        try{
            $service->assert($drink, $money);
            $this->assertFalse(true, "No exception thrown!");
        }catch (\Exception $e){
            $this->assertEquals($message, $e->getMessage());
        }
    }

    public function testThat_when__money_is_not_enough_for_tea_return_Exception()
    {
        //GIVEN
        $drink = Drink::TEA;
        $money = 0.22;
        $message = Drink::$names[$drink]." costs ". Drink::$prices[$drink] . ", 0.18 is missing";
        //WHEN
        $service = new AssertEnoughMoneyIsProvidedService();
        try{
            $service->assert($drink, $money);
            $this->assertFalse(true, "No exception thrown!");
        }catch (\Exception $e){
            $this->assertEquals($message, $e->getMessage());
        }
    }

    public function testThat_when_money_is_not_enough_for_choco_return_Exception()
    {
        //GIVEN
        $drink = Drink::CHOCO;
        $money = 0.25;
        $message = Drink::$names[$drink]." costs ". Drink::$prices[$drink] . ", 0.25 is missing";
        //WHEN
        $service = new AssertEnoughMoneyIsProvidedService();
        try{
            $service->assert($drink, $money);
            $this->assertFalse(true, "No exception thrown!");
        }catch (\Exception $e){
            $this->assertEquals($message, $e->getMessage());
        }
    }

}