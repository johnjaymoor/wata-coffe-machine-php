<?php

require_once (dirname(__FILE__).'/drink/DrinkService.php');
require_once (dirname(__FILE__).'/drink/GenerateInstructionsForDrinkMakerServiceI.php');
require_once (dirname(__FILE__).'/drink/GenerateInstructionsForDrinkMakerService.php');
require_once (dirname(__FILE__).'/message/MessageServiceI.php');
require_once (dirname(__FILE__).'/message/MessageService.php');
require_once (dirname(__FILE__).'/model/Order.php');
require_once (dirname(__FILE__).'/model/Drink.php');
require_once (dirname(__FILE__).'/stock/StockServiceI.php');
require_once (dirname(__FILE__).'/stock/StockService.php');
require_once (dirname(__FILE__).'/stock/LoadStockServiceI.php');
require_once (dirname(__FILE__).'/stock/LoadStockService.php');
require_once (dirname(__FILE__).'/stock/SaveStockServiceI.php');
require_once (dirname(__FILE__).'/stock/SaveStockService.php');
require_once (dirname(__FILE__).'/validator/AssertEnoughMoneyIsProvidedServiceI.php');
require_once (dirname(__FILE__).'/validator/AssertEnoughMoneyIsProvidedService.php');

use \Src\Model\Order;
use \Src\Model\Drink;
use \Src\Drink\DrinkService;
use \Src\Drink\GenerateInstructionsForDrinkMakerService;
use \Src\Message\MessageService;
use \Src\Validator\AssertEnoughMoneyIsProvidedService;
use \Src\Stock\StockService;
use \Src\Stock\LoadStockService;
use \Src\Stock\SaveStockService;

$order1 = new Order(Drink::COFFEE, 1, 0.6, false);
$order2 = new Order(Drink::TEA, 1, 0.4, false);
$order3 = new Order(Drink::CHOCO, 1, 0.5, true);
$order4 = new Order(Drink::ORANGE, 1, 0.7, false);
$order41 = new Order(Drink::ORANGE, 0, 0.7, false);
$order5 = new Order(Drink::COFFEE, 1, 0.3, false);
$order6 = new Order(Drink::TEA, 1, 0.3, false);
$order7 = new Order(Drink::ORANGE, 1, 0.3, false);

$stockService = new StockService(new LoadStockService(), new SaveStockService());
$drinkService = new DrinkService(
    new AssertEnoughMoneyIsProvidedService(),
    new MessageService(),
    new GenerateInstructionsForDrinkMakerService(),
    $stockService
);

echo $drinkService->order($order1);
echo "\n";
echo $drinkService->order($order2);
echo "\n";
echo $drinkService->order($order3);
echo "\n";
echo $drinkService->order($order4);
echo "\n";
echo $drinkService->order($order41);
echo "\n";
echo $drinkService->order($order5);
echo "\n";
echo $drinkService->order($order6);
echo "\n";
echo $drinkService->order($order7);
echo "\n";
$stockService->printSellingReport();
echo "\n";