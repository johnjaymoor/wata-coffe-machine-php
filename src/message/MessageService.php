<?php

namespace Src\Message;

class MessageService implements MessageServiceI {
    public const MESSAGE_PREFIX = "M";
    public function sendMessage(string $message){
            return self::MESSAGE_PREFIX.":".$message;
    }
}