<?php


namespace Src\Message;


interface MessageServiceI
{
    public function sendMessage(string $message);
}