<?php


namespace Src\Validator;


use Src\Model\Drink;

class AssertEnoughMoneyIsProvidedService implements AssertEnoughMoneyIsProvidedServiceI
{

    public function assert(string $drink, float $money)
    {
        if (in_array($drink,array_keys(Drink::$names)) &&
            $money < Drink::$prices[$drink]
        ) {
            throw new \Exception(Drink::$names[$drink] . " costs " . Drink::$prices[$drink] . ", "
                . number_format(Drink::$prices[$drink] - $money, 2) . " is missing");
        }
    }
}