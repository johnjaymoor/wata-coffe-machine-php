<?php


namespace Src\Validator;

interface AssertEnoughMoneyIsProvidedServiceI
{

    public function assert(string $drink, float $money);
}