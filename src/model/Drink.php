<?php

namespace Src\Model;

class Drink
{
    const COFFEE = "C";
    const TEA = "T";
    const CHOCO = "H";
    const ORANGE = "O";
    const EXTRA_HOT = "h";

    public static $names = [
        self::COFFEE => "Coffee",
        self::TEA => "Tea",
        self::CHOCO => "Chocolate",
        self::ORANGE => "Orange juice"
    ];

    public static $prices = [
        self::COFFEE => 0.6,
        self::TEA => 0.4,
        self::CHOCO => 0.5,
        self::ORANGE => 0.6
    ];
}