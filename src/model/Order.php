<?php

namespace Src\Model;

class Order{

    private $drink;
    private $sugar;
    private $money;
    private $extraHot;

    public function __construct(
        string $drink,
        int $sugar,
        float $money,
        bool $extraHot
    )
    {
        $this->drink = $drink;
        $this->sugar = $sugar;
        $this->money = $money;
        $this->extraHot = $extraHot;
    }

    public function getDrink():string{
        return $this->drink;
    }

    public function getSugar():int{
        return $this->sugar;
    }

    public function getMoney(): float
    {
        return $this->money;
    }

    public function isExtraHot():bool
    {
        return $this->extraHot;
    }

}