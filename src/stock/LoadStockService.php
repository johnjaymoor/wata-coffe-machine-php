<?php


namespace Src\Stock;


class LoadStockService implements LoadStockServiceI
{

    public function load(): array
    {
       $array =   array_map(
           'str_getcsv',
           file(dirname(__FILE__)."/stock.csv")
       );
       return $array[0];
    }
}