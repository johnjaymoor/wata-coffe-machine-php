<?php


namespace Src\Stock;


interface LoadStockServiceI
{
    public function load():array;
}