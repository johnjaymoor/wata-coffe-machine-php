<?php


namespace Src\Stock;


interface SaveStockServiceI
{
    public function save(array $array);
}