<?php


namespace Src\Stock;


interface StockServiceI
{
    public function sell(string $drink):void;
    public function printSellingReport();
}