<?php


namespace Src\Stock;


use Src\Model\Drink;

class StockService implements StockServiceI
{
    private const DELIMITER = ":";
    private const TOTAL_TEXT = "Total";

    private $loadStockService;
    private $saveStockService;

    public function __construct(
        LoadStockServiceI $loadStockService,
        SaveStockServiceI $saveStockService
    )
    {
        $this->loadStockService = $loadStockService;
        $this->saveStockService = $saveStockService;
    }

    public function sell(string $drink): void
    {
        $stock = $this->loadStockService->load();
        $drinkIndex = array_search($drink, array_keys(Drink::$names));
        $stock[$drinkIndex] += 1;
        $this->saveStockService->save($stock);
    }

    public function printSellingReport()
    {
        $stock = $this->loadStockService->load();
        $index = 0;
        $total = 0;
        foreach (Drink::$names as $key => $name) {
            echo $name . self::DELIMITER . $stock[$index] . " ";
            $total += $stock[$index] * Drink::$prices[$key];
        }
        echo self::TOTAL_TEXT . self::DELIMITER . number_format($total, 2);

    }
}