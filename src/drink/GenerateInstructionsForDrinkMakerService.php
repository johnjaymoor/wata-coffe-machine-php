<?php


namespace Src\Drink;


use Src\Model\Drink;

class GenerateInstructionsForDrinkMakerService implements GenerateInstructionsForDrinkMakerServiceI
{
    const SEPARATOR = ":";

    public function generate(string $drink, int $sugar, bool $extraHot): string
    {
        $extraHotCode = "";
        $sugarCode = "";
        $stickCode = "";
        if (Drink::ORANGE != $drink) {
            $extraHotCode = $extraHot ? Drink::EXTRA_HOT : "";
            $sugarCode = $sugar == 0 ? "" : $sugar;
            $stickCode = $sugar == 0 ? "" : "0";
        }
        return $drink . $extraHotCode . self::SEPARATOR . $sugarCode . self::SEPARATOR . $stickCode;
    }
}