<?php


namespace Src\Drink;


use Src\Model\Drink;

interface GenerateInstructionsForDrinkMakerServiceI
{

    public function generate(string $drink, int $sugar, bool $extraHot): string;
}