<?php

namespace Src\Drink;

use Src\Message\MessageServiceI;
use Src\Model\Order;
use Src\Stock\StockServiceI;
use Src\Validator\AssertEnoughMoneyIsProvidedServiceI;

class DrinkService
{
    private $assertEnoughMoneyIsProvidedService;
    private $messageService;
    private $generateInstructionsForDrinkMakerService;
    private $stockService;

    public function __construct(
        AssertEnoughMoneyIsProvidedServiceI $assertEnoughMoneyIsProvidedService,
        MessageServiceI $messageService,
        GenerateInstructionsForDrinkMakerServiceI $generateInstructionsForDrinkMakerService,
        StockServiceI $stockService
    )
    {
        $this->assertEnoughMoneyIsProvidedService = $assertEnoughMoneyIsProvidedService;
        $this->messageService = $messageService;
        $this->generateInstructionsForDrinkMakerService = $generateInstructionsForDrinkMakerService;
        $this->stockService = $stockService;
    }

    public function order(Order $order): string
    {
        try {
            $this->assertEnoughMoneyIsProvidedService->assert(
                $order->getDrink(),
                $order->getMoney()
            );
            $this->stockService->sell($order->getDrink());
            return $this->generateInstructionsForDrinkMakerService->generate(
                $order->getDrink(),
                $order->getSugar(),
                $order->isExtraHot()
            );
        } catch (\Exception $e) {
            return $this->messageService->sendMessage($e->getMessage());
        }

    }

}